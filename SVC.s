    PRESERVE8
    THUMB
    
    AREA    |.text|, CODE, READONLY

SVC_Handler  PROC
                EXPORT  SVC_Handler
                IMPORT  nextTask;
                    
                cpsid i ;disable irq   
                ldr r0, =nextTask ;load address of next task pointer
                ldr r0, [r0] ;dereference
                ldr r0, [r0]
                
                ldmfd r0!, {r4-r11, lr}
                                
                msr psp, r0
                
                cpsie i ;enable irq
                
                bx lr
               
                ENDP
    END