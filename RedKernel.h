#ifndef redkernel_h
#define redkernel_h

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

    
void TaskCreate(uint8_t *stack, uint32_t stackSize, void (*entrypoint)());
    
void KernelStart(void);
void Tick(void);
void TaskYield(void);

    
#ifdef __cplusplus
}
#endif

#endif
