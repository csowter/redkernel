    PRESERVE8
    THUMB
    
    AREA    |.text|, CODE, READONLY
		
PendSV_Handler  PROC
                EXPORT  PendSV_Handler
                IMPORT currentTask
			    IMPORT nextTask
                ;push software stack frame onto psp
				;load next task
				;pop software stack frame from stored sp
				;load psp with tasks sp
				;lr at when entering exception is a return code to say whether to 
				;unstack an FPU context when leaving exception handler, it needs to be 
				;stored separately from the lr which was stacked by hardware on exception entry
				cpsid i
				
				mrs r0, psp
				
				ldr r1, =currentTask
				ldr r2, =nextTask
				
				ldr r1, [r1]
				ldr r2, [r2]
				
				cmp r1, r2 ;are we switching back to the same task? 
				beq ExitContextSwitch ;exit early if so
				
				tst lr, #0x00000010 ;was this a floating point context?
				it      eq
				vstmdbeq  r0!, {s16-s31} ;stack fpu registers not done by hardware
				
				stmfd r0!, {r4-r11, lr}
				str r0, [r1] ;store stack pointer in current task
				
				;switch tasks
				
                ldr r0, [r2]  ;load sp from next task
                
                ldmfd r0!, {r4-r11, lr}
				
				tst     lr, #0x00000010 ; do we need to restore an fpu context?
				it      eq
				vldmiaeq  r0!,      {s16-s31}
				
				msr psp, r0
				
ExitContextSwitch
				cpsie i
                bx lr
                nop
                ENDP
    END